# Monkey Hear 🐵

This is my implementation of the Monkey language project laid out in Thorsten Ball's fun "Writing an Interpreter in Go" book.

Acquire the book here:
- https://gumroad.com/l/waiig
- https://interpreterbook.com/